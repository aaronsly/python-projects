import PyPDF2, csv, os


def app():

    directory = os.fsencode('tmp/')

    for file in os.listdir(directory):

        print(file)
        filename = 'tmp/' + os.fsdecode(file)
        print(filename)

        if 'Legacy' in filename:
            continue
        
        if filename.endswith(".pdf"):
            pdfFileObj = open(filename, 'rb')
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
            num_pages = pdfReader.numPages
            count = 0
            
            #The while loop will read each page
            text = ""
            while count < num_pages:   
                pageObj = pdfReader.getPage(count)
                count += 1
                text += pageObj.extractText()
                #This if statement exists to check if the above library returned #words. It's done because PyPDF2 cannot read scanned files.        
            
            
            with open('tmp/csv/{}.csv'.format(os.fsdecode(file).replace('.pdf',"")), 'w') as csv_file:
                if text != "":
                    writer = csv.writer(csv_file)
                    for page in text.split('PRODUCT SPECIFICATION'): 
                        product = []
                        for item in page.split('\n'):
                            if item in ['IMAGE', 'DRG']:
                                continue
                            else:
                                product.append(item.replace('\u0141', 'L').replace(
                                    '\u02da', "").replace('\ufb02',""))
                        writer.writerow(filter(None, product))

        else:
            continue


    # #write a for-loop to open many files -- leave a comment if you'd #like to learn how
    # filename = 'tmp/SHO028PR-Spec.pdf'
    # #open allows you to read the file
    # pdfFileObj = open(filename, 'rb')
    # #The pdfReader variable is a readable object that will be parsed
    # pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    # #discerning the number of pages will allow us to parse through all #the pages
    # num_pages = pdfReader.numPages
    # count = 0
    # text = ""
    # #The while loop will read each page
    # while count < num_pages:
    #     pageObj = pdfReader.getPage(count)
    #     count += 1
    #     text += pageObj.extractText()
    # #This if statement exists to check if the above library returned #words. It's done because PyPDF2 cannot read scanned files.
    # if text != "":
    #     text = text

    # print(text.split('\n'))

    # with open('test.csv', 'w') as csv_file:
    #     writer = csv.writer(csv_file)        
    #     writer.writerow(text.split('\n'))


if __name__ == "__main__":
    app()
