import requests, csv, sys, os, random, re, glob, errno, time, stem, xml.etree.ElementTree as ET
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
from datetime import datetime, timedelta
from stem import Signal
from stem.control import Controller

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port=9051) as controller:
        if controller.is_newnym_available():
            controller.authenticate(password="essentialWebScrape!")            
            controller.signal(Signal.NEWNYM)            
            time.sleep(controller.get_newnym_wait())

def progress(count, total, prefix = '',suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix))
    sys.stdout.flush()  
    if count == total:
        print()
       

def main():
    clear()    
    with Controller.from_port(port=9051) as controller:
        if controller.is_alive():
            ua = UserAgent().random                
            session = get_tor_session()                
            tree = ET.parse("kartell-sitemap.xml")
            root = tree.getroot()
            count = 0
            for child in root:                    
                for subelem in child:
                    if subelem.tag == 'loc' and subelem.text.find("pdfs") != -1:                            
                        url = subelem.text
                        filename = url.split("pdfs/",1)[1].replace("%20", "-")
                        print(url)
                        print(filename)
                        if count % 2 == 0 and count != 0:                                 
                            ua = UserAgent().random
                            renew_connection()
                            session = get_tor_session()
                        
                        response = session.get(url, headers={'user-agent': ua}, timeout=30)
                        if 200 == response.status_code: 
                            with open('tmp/' + filename, 'wb') as f:
                                f.write(response.content)
main()
