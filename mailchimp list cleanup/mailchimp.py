import csv, os,sys

os.system('cls')
print('')
def get_emails(folder,file_name):
    out = []
    with open('./{}/{}.csv'.format(folder,file_name), 'r') as f:    
        for subscriber in csv.reader(f):
            if subscriber[0] != 'Email Address':
                out.append(subscriber[0])
    return out

es_subscribed = get_emails('es newsletter','es-subscribed')
es_unsubscribed = get_emails('es newsletter','es-unsubscribed')
bb_subscribed = get_emails('bb newsletter','bb-subscribed')
bb_unsubscribed = get_emails('bb newsletter','bb-unsubscribed')
hb_subscribed = get_emails('hb newsletter','hb-subscribed')
hb_unsubscribed = get_emails('hb newsletter','hb-unsubscribed')

for email in bb_subscribed:
    if email in es_subscribed:
        print(email)
print('')
print(es_subscribed)
print(os.path.dirname(sys.executable))
