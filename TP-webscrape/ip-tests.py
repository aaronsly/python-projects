import requests, random, os, json
from fake_useragent import UserAgent
from pprint import pprint
from bs4 import BeautifulSoup as BS


def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def get_proxies():
    proxies = []
    session = requests.session()
    ua = UserAgent().random
    print(ua)
    proxies_response = session.get("https://www.socks-proxy.net/", headers={'user-agent': ua})

    if proxies_response.status_code == 200:
        proxy_soup = BS(proxies_response.content, "html.parser")
        proxies_table = proxy_soup.find(id='proxylisttable')

        # Save proxies in the array
        for row in proxies_table.tbody.find_all('tr'):
            cells = row.find_all('td')
            #if cells[4].text == 'Socks5':
            proxies.append({
            'ip':   cells[0].string,
            'port': cells[1].string,
            'socks': cells[4].string
            })
    return proxies


def random_proxy(proxies):
  return random.randint(0, len(proxies) - 1)


def get_session(proxy):

    session = requests.session()
    session.proxies = {'http':  '{}://{}:{}'.format(proxy['socks'],proxy['ip'], proxy['port']),
                       'https': '{}://{}:{}'.format(proxy['socks'],proxy['ip'], proxy['port'])}
    return session


clear()
proxies = get_proxies()
proxy_index = random_proxy(proxies)

for i in range(0,100):
    if i % 10 == 0:
        proxy = proxies[proxy_index]
        session = get_session(proxy)
        ua = UserAgent().random
    ip_json = json.loads(session.get("http://httpbin.org/ip", headers={'user-agent': ua}).text)
    print(ip_json['origin'])
    print(session.get("http://httpbin.org/headers", headers={'user-agent': ua}).text)
