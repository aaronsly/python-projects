import requests, os, json, time, sys, subprocess
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
from stem import Signal
from stem.control import Controller

# Make sure tor is running, run the below command in the cmd
# C:/Users/AaronSly/TorBrowser/Tor/tor.exe -f ../Data/Tor/torrc | more

def kill_tor():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate(password="essentialWebScrape!")
        controller.signal(Signal.TERM)
        print("Tor killed")

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session


def renew_connection():    
    with Controller.from_port( port = 9051 ) as controller:
        if controller.is_newnym_available():
            controller.authenticate(password="essentialWebScrape!")
            print("Success!")
            controller.signal(Signal.NEWNYM)            
            print("Current wait")
            print(controller.get_newnym_wait())
            print("New tor connection!")
            time.sleep(controller.get_newnym_wait())

clear()

ua = UserAgent().random
session = get_tor_session()

print(session.get("http://httpbin.org/ip", headers={'user-agent': ua}).text)
# print(session.get("http://httpbin.org/headers",headers={'user-agent': ua}).text)

for i in range(0, 50):

    if i % 10 == 0 and i != 0:        
        ua = UserAgent().random
        renew_connection()
        session = get_tor_session()

    ip_json = json.loads(session.get("http://httpbin.org/ip", headers={'user-agent': ua}).text)
    print(ip_json['origin'])

# kill_tor()
