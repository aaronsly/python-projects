import requests, os
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS


def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

clear()
session = requests.session()
request = session.get("http://www.nationsonline.org/oneworld/country_code_list.htm", headers={'user-agent': UserAgent().random})

if request.status_code == 200:
    page_soup = BS(request.content, "html.parser") 
    table = page_soup.find(id="codelist")    

    for row in table.find_all("tr", class_="border1"):
        has_country = row.find("td", class_="abs")
        if has_country:
            name = has_country.text
            code = has_country.find_next_sibling('td').text
        else:
            name = ''
            code = ''

        if code == 'GB':
            print('<option value="{}" selected>{} - {}</option>'.format(code, name, code))
        else:            
            print('<option value="{}">{} - {}</option>'.format(code, name, code))
