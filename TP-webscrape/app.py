import requests, csv, sys, os, random, re, glob, errno, time, stem
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS
from datetime import datetime, timedelta
from stem import Signal
from stem.control import Controller

#tor.exe -f ../Data/Tor/torrc | more
def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def progress(count, total, prefix = '',suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix))
    sys.stdout.flush()  
    if count == total:
        print()

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_connection():
    with Controller.from_port(port=9051) as controller:
        if controller.is_newnym_available():
            controller.authenticate(password="essentialWebScrape!")            
            controller.signal(Signal.NEWNYM)            
            time.sleep(controller.get_newnym_wait())

def parseProduct(soup, url) :
    product = {}
    try:
        overview_tab = soup.select_one("#tab-overview")
        specs_tab = soup.select_one("#tab-techspecs")
        data_sheets = soup.select_one("#tab-datasheets")
        cat = soup.find("li", class_="active")
        product_name = soup.select_one('.tpProductTitle')
        product_code = soup.select_one('.tpProductItem > span')        
        price_ex_vat = soup.select_one('.price_value')
        price_inc_vat = soup.select_one('.includedVAT')
        hire_inc_vat = soup.select_one('.including-vat.tab')
        hire_ex_vat = soup.select_one('.excluding-vat.tab')

        if cat:
            product['category'] = cat.find_previous_sibling("li").text.strip()
        else:
            product['category'] = "No Category"

        if product_name:
            product['product_name'] = product_name.text.strip()
        else:
            product['product_name'] = "No Product Name"

        if product_code:
            product['product_code'] = product_code.text.strip()
        else:
            product['product_code'] = "No Product Code"
        
        if price_ex_vat:
            product['price_ex_vat'] = price_ex_vat.text.strip().replace("£","")
        elif hire_ex_vat:
            product['hire_ex_vat'] = hire_ex_vat.select_one('.price-value').text.strip().replace("£", "")
        else:
            product['price_ex_vat'] = "N/A"
        
        if price_inc_vat:
            product['price_inc_vat'] = price_inc_vat.text.strip().replace("£", "")
        elif hire_inc_vat:
            product['hire_inc_vat'] = hire_inc_vat.select_one('.price-value').text.strip().replace("£", "")
        else:
            product['price_inc_vat'] = "N/A"        

        if overview_tab:
            product['overview'] = overview_tab.get_text().replace("\n\n", "").strip()

        if specs_tab:
            for row in specs_tab.find_all("tr"):
                product[row.select_one(".attrib").text.strip().lower()] = row.find(attrs={'class': None}).text.strip()
        
        if data_sheets:
            links = ''
            for sheet in data_sheets.find_all("a"):
                links += sheet['href'] + "\n"
            product['data_sheets'] = links

    except AttributeError as e:
        print(e, url)

    return product

def createUrlFiles():
    session = get_tor_session()
    ua = UserAgent().random
    request = session.get("https://www.travisperkins.co.uk/sitemap.xml",headers={'user-agent': ua})

    if 200 != request.status_code:
        return False

    urls = BS(request.content, "html.parser").findAll('url')

    if not urls:
        return False

    url_counter = 0
    products = {}
    print("Total Urls: {}".format(len(urls)))
    progress(url_counter, len(urls), "Getting urls", "Complete")
    for u in urls:
######################################## vvv REMOVE THIS WHEN FINSISHED vvv ###################################################
        # if url_counter == 500:
        #     break
######################################## ^^^ REMOVE THIS WHEN FINSISHED ^^^ ###################################################
        # if url_counter % random.choice([9,13,21,50,100,250]) == 0 and url_counter != 0:
        if url_counter % random.choice([50, 100, 250]) == 0 and url_counter != 0:
            #time.sleep(random.choice([1,2,3,4]))
            ua = UserAgent().random
            renew_connection()
            session = get_tor_session()

        loc = u.find('loc').text
        # get rid of category pages
        if loc.find('Product') > 0:
            continue
        try:   
        # check for child products and get category
            product_page = session.get(loc, headers={'user-agent': ua}, timeout=10)
            if 200 == product_page.status_code: 
                soup = BS(product_page.content, "html.parser")
                variants = soup.select_one('.tpVariantsWrapper')
                try:
                    main_cat = soup.select_one('.tpBreadcrumb').find("a", string=" Product").parent.find_next_sibling("li").get_text().strip()
                except AttributeError as e:  
                    main_cat = "Uncategorised"
                    print('')
                    print(e)
                
                try:
                    cat = soup.select_one('#content').find("li", class_="active").find_previous_sibling("li").text.strip()
                except AttributeError as e:  
                    cat = "Uncategorised"
                    print('')
                    print(e)
               
                key = main_cat + '~' + cat
                # add parent product to list
                if key not in products:
                    products[key] = [loc]
                else:
                    products[key].append(loc)
                # add child products
                if variants:
                    for option in variants.find_all('option'):
                        if option['value']:
                            products[key].append("https://www.travisperkins.co.uk" + option['value'])

                url_counter += 1
                progress(url_counter, len(urls), "Getting Urls", "Complete")
                # progress(url_counter, 500, "Getting urls", "Complete")
            else:
                print('')
                print(product_page.status_code)
                print(loc)
                print(session.get("http://httpbin.org/ip", headers={'user-agent': ua}).text)
                url_counter += 1
                progress(url_counter, len(urls), "Getting Urls", "Complete")

        except requests.exceptions.ConnectionError as err:
            print("\nError connecting site : {} \n".format(err))  
            print(loc)
        except requests.exceptions.ReadTimeout as e:
            print("\nError connecting site : {} \n".format(e))
            print(loc)
        except requests.exceptions.ChunkedEncodingError as e:
            print("\nSite response delayed, skipping retrieval..: {}".format(e))
            print(loc)

    if not os.path.exists('./urls'):
        os.makedirs('./urls')
    
    file_count = 0
    progress(file_count, len(products), "Creating csv files", "Complete")
    for cat, cat_products in products.items():

        if not os.path.exists('./urls/' + cat.split('~')[0]):
            os.makedirs('./urls/' + cat.split('~')[0])

        csv_file = 'urls/{}.csv'.format(cat.replace('~','/'))
        # create and write to file based on category
        with open(csv_file, "w", newline="") as f:
            writer = csv.writer(f)
            for link in cat_products:
                writer.writerow([link,])

        file_count += 1
        progress(file_count, len(products), "Creating csv files", "Complete")

    return True

def findCategoryHeadings(cat):
    headings = []
    for product in cat:
        # find headings
        for key in product.keys():
            if key not in headings:
                headings.append(key)
    return headings

def checkUrls():
    if not os.path.exists('./urls') or len(os.listdir('./urls')) == 0:
        build = input("""There are currently no urls stored to scrape would you like to create those files now? (This may take some time)\n
                      Press enter/return to start, or enter Q to quit\n >> """).lower()
        if build == 'q':
            print("Bye")
            sys.exit()
        else:
            if createUrlFiles():
                return True
            else:
                return False
    else:
        directory = './urls/{}/'.format(os.listdir('./urls')[0])     
        check_file = directory + os.listdir(directory)[0]
        file_mod_time = datetime.fromtimestamp(os.stat(check_file).st_mtime)

        if datetime.today() - file_mod_time > timedelta(days=7):
            print("""The urls stored to scrape are older than 7 days would you like to update the files now? (This may take some time)\r
Press enter/return to update the urls, or enter C to continue with old urls or enter Q to quit""")
            while True:
                rebuild = input(">> ").lower()
                if rebuild == 'c':
                    return True
                elif rebuild == 'q':
                    sys.exit()
                elif rebuild == '':            
                    if createUrlFiles():
                        return True
                    else:                        
                        return False
                else:
                    print("Please enter a vaild value.")
        else:
            return True

def getCategories():
    cats = os.listdir('./urls')
    print("""***********************************************************\r
* What category(s) would you like to scrape?
* Enter the category number(s) separated by a ','
* To scrape all categories enter 'all'
***********************************************************\n""")
    cats_to_scrape = []
    for cat in cats:
        print(cats.index(cat), " - ", cat)

    while True:
        cats_input = input("\n>> ").lower()
        ok_cats = True
        if cats_input == 'all':
            for cat in cats:
                cats_to_scrape.append(cats.index(cat))
        else:
            try:
                for cat_index in cats_input.split(','):
                    if cat_index:
                        i = int(re.sub(r"\D", "", cat_index))
                        if i >= len(cats):
                            ok_cats = False
                            print(
                                "There is no category {} please re-enter the category(s) you would like to scrape.".format(i))
                            break
                        elif cat_index != '' and i not in cats_to_scrape:
                            cats_to_scrape.append(int(i))
            except ValueError:
                print("Please check your input")
                ok_cats = False

        if ok_cats == True:
            break

    return cats_to_scrape

def getProductUrlsbyCategory(categories):
    products = {}
    paths = []
    for i in categories:
        cat_list = os.listdir('./urls')
        paths.append('/'.join(['./urls', cat_list[i], '']))

    for path in paths:
        for name in glob.glob(path + '*.csv'):
            try:
                with open(name, 'r') as f:
                    key = path.replace('./urls/', '').replace('/', '')
                    urls = csv.reader(f)
                    for url in urls:
                        if key not in products:
                            products[key] = [url[0]]
                        else:
                            products[key].append(url[0])
            except IOError as exc:
                if exc.errno != errno.EISDIR:  # Do not fail if a directory is found, just ignore it.
                    raise  # Propagate other kinds of IOError.
    return products

def scrapeSelectedProducts(urls):
    errors = []
    products = {}
    print('')
    for category, prod_urls in urls.items(): 

        url_count = 0
        products[category] = []
        ua = UserAgent().random
        session = get_tor_session()

        print("{} - {} pages".format(category,len(prod_urls)))
        progress(url_count, len(prod_urls), "Scraping pages", "Complete")
        for product_url in prod_urls:

            if url_count % random.choice([6, 10, 13, 21, 50, 100, 250]) == 0 and url_count != 0:                
                #time.sleep(random.choice([1,2,3,4]))
                ua = UserAgent().random
                renew_connection()
                session = get_tor_session()
                progress(url_count, len(prod_urls), "Scraping pages", "Complete")
            try:
                request = session.get(product_url, headers={'user-agent': ua}, timeout=10)            
                if request.status_code == 200:
                    # if category not in products:
                    #     products[category] = [scrapeProduct(BS(request.content, "html.parser"), product_url)]
                    # else:                    
                    products[category].append(parseProduct(BS(request.content, "html.parser"), product_url))
                else:
                    error_string = "Failed to scrape page: " + product_url + "\nServer Response: {}".format(request.status_code)
                    errors.append(error_string)
                url_count = url_count + 1
                progress(url_count, len(prod_urls), "Scraping pages", "Complete")
            except requests.exceptions.ConnectionError as e:
                errors.append("\nError connecting site : {} \nUrl: {}".format(e, product_url))
            except requests.exceptions.ReadTimeout as e:
                errors.append("\nError connecting site : {} \nUrl: {}".format(e, product_url))
            except requests.exceptions.ChunkedEncodingError as e:
                errors.append("\nError connecting site : {} \nUrl: {}".format(e, product_url))    
    if errors:
        print(errors)
    print('')
    return products

def sortProductsbyCat(prods):
    categories = {}
    for cat,products in prods.items():
        categories[cat] = {}
        for product in products:
            key = product['category']
            if key not in categories[cat]:
                categories[cat][key] = [product]
            else:
                categories[cat][key].append(product)

    return categories

def saveProductsToCsv(products):
    if not os.path.exists('./output'):
        os.makedirs('./output')

    for category, sub_categories in products.items():
        path = './output/{}'.format(category)
        if not os.path.exists(path):
            os.makedirs(path)
        file_count = 0
        print(category)
        progress(file_count, len(sub_categories), "Creating Files", "Complete")
        for sub_category, items in sub_categories.items():
            file_name = path + '/TP Products - {}.csv'.format(sub_category)
            # find file headings
            headings = findCategoryHeadings(items)
            rows = []
            # add products by row
            for item in items:
                row = {}
                # sort product details for file insertion
                for heading in headings:
                    if heading in item:
                        row[heading] = item[heading]
                    else:
                        row[heading] = ''
                rows.append(row)

            try:
                # create and write to file based on category
                with open(file_name, "w", newline="") as f:
                    writer = csv.DictWriter(f, headings)
                    writer.writeheader()
                    writer.writerows(rows)
                file_count = file_count + 1
                progress(file_count, len(sub_categories),"Creating Files", "Complete")
            except PermissionError as e:
                print(e)
                output =  False
            else:
                output = True
        print('')
    return output

def welcome():
    try:
        with Controller.from_port(port=9051) as controller:
            if controller.is_alive():
                print("""\n***********************************************************
*      Welcome To The Travis Perkins Product Scraper      *
***********************************************************\n""")
            else:
                print("""Tor connection is not active please check connection and try again.""")
                sys.exit()
    except stem.SocketError:
        print("""This program uses Tor which is not currently active.
Please connect to the Tor Network with ControlPort 9051 enabled and try again.""")
        sys.exit()
        

def main():
    clear()
    welcome()
    if checkUrls():
        categories = getCategories()
        product_urls = getProductUrlsbyCategory(categories)
        products = scrapeSelectedProducts(product_urls)
        scraped_products = sortProductsbyCat(products)
        result = saveProductsToCsv(scraped_products)
    if result:
        print("""\n***********************************************************
* Would you like to scrape some more products?
* Press enter/return to continue or Q to quit
***********************************************************""")
        while True:
            rerun = input(">> ").lower()
            if rerun == '':
                main()
            elif rerun == 'q':
                clear()
                sys.exit()
            else:
                print("Please check your input.")

if __name__ == "__main__":    
    main()
