import requests, csv
from bs4 import BeautifulSoup

file_name = input("What would you like to name the file? ")
site = "https://www.theheatingboutique.co.uk/"
urls = ["carisa-environ-stainless-steel-designer-heated-towel-rail-850mm-x-400mm.html",
        "hb-essentials-zena-white-straight-ladder-heated-towel-rail-800mm-x-400mm-dual-fuel-non-thermostatic.html",
        "hb-essentials-zeno-chrome-curved-ladder-heated-towel-rail-600mm-x-400mm-central-heating.html",
        "lazzarini-siracusa-chrome-designer-heated-towel-rail-730mm-x-500mm-central-heating.html", ]
headers = ["Product Name", "Sku", "Old Price", "Price", "You Save"]
rows = []

for url in urls:
    request = requests.get(site + url)    

    if request.status_code == 200:
        soup = BeautifulSoup(request.content, "html.parser")
        container = soup.select_one(".product-view")
        old_price_container = container.select_one(".old-price")
        price_container = container.select_one(".special-price")       

        if(old_price_container):
            raw_old_price = old_price_container.select_one(".price").text.strip()
            old_price = raw_old_price.replace("£", "").replace(",", "")
        else :
            old_price = ""

        if( price_container ):
            raw_special_price = price_container.select_one(".price").text.strip()
            price = raw_special_price.replace("£", "").replace(",","")
        else:
            price = ""

        row = { 
            "Product Name": container.select('h1[itemprop="name"]')[0].text,
            "Sku": container.select('span[itemprop="sku"]')[0].text,
            "Old Price" : old_price,
            "Price": price,
            "You Save": container.select_one(".savings-price").text.replace("£", "").replace(",", "")
        }

        table_rows = container.table.find_all("tr")
        for spec in table_rows:
            label = spec.select_one(".label").text.strip()

            if label not in headers:
                headers.append(label)

            row[label] = spec.select_one(".data").text.strip() 
        
        rows.append(row)
    else:
        print("Server Response: {}".format(request.status_code))
        print("Failed to scrape page: " + url)

# write to file
with open(file_name + ".csv", "w", newline="") as f:
    writer = csv.DictWriter(f,headers)
    writer.writeheader()
    writer.writerows(rows)