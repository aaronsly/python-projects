import requests
from bs4 import BeautifulSoup

num_pages = int( input("Number of pages to scrape? ") )
cat = input("What would you like to name the file? ")
file_name = cat + ".csv"
f = open(file_name, "w")
f.write("Product Name, Old Priced From, Special Priced From\n")

for i in range(num_pages):
    url = "https://www.theheatingboutique.co.uk/heated-towel-rails?p=" + str(i+1)    
    request = requests.get(url)
    status = request.status_code    

    if status == 200 : 
        soup = BeautifulSoup(request.content, "html.parser")

        #grabs each product
        containers = soup.find_all("div", {"class":"item"})       

        # Loop over the products
        for container in containers:        
            product_name = container.h2.a.text
            old_price = container.select_one(".old-price")
            special_price = container.select_one(".special-price")

            if( old_price ):
                raw_old_price = old_price.select_one(".price").text.strip()
                old_price_from = raw_old_price.replace("£", "").replace(",", "")                
            else :
                old_price_from = ""

            if( special_price ):
                raw_special_price = special_price.select_one(".price").text.strip()
                special_price_from = raw_special_price.replace("£", "").replace(",","")
            else:
                special_price_from = ""

            f.write(product_name + "," + old_price_from + "," + special_price_from + "\n")
        print("Page " + str(i+1) + " done.")
    else :    
        print( "Server Response: {}".format(status) )

f.close()
print("Complete: " + file_name + " created.")
