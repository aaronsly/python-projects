import sys
import os
import re


def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')


def progress(count, total, prefix='', suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('%s [%s] %s%s %s\r' %
                     (prefix, bar, percents, '%', suffix))
    sys.stdout.flush()
    if count == total:
        print()


def str_to_int(s):
    if isinstance(s, str):
        if s:
            s = s.replace(" ", "")
            s = ''.join(filter(lambda x: x.isdigit(), s))
        else:
            s = 0
    return s


def search_dict_list(needle, haystack, key):
    for item in haystack:
        if item[key] == needle:
            return True

    return False


def remove_html_tags(text):
    """Remove html tags from a string"""
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


def price_str_to_int(price):
    for ch in ["£", ",", "Inc VAT", "€", "Â"]:
        if ch in price:
            price = price.replace(ch, "")

    return float(price)


def replace_all(chars, replacement, string):
	for ch in chars:
		if ch in string:
			string = string.replace(ch, replacement)
	return string


def findCategoryHeadings(product):
    headings = []
    # find headings
    for key in product.keys():
        if key not in headings:
            headings.append(key)
    return headings
