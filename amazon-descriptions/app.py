import helper, csv, os, io
from bs4 import BeautifulSoup as BS

def app():
	helper.clear()

	print("working")
	file = 'AmazonDescriptionSummary.csv'
	rows = []
	with open(file) as fh:		
		rd = csv.DictReader(fh, delimiter=',')
		for row in rd:
			product = {
				'Inventory Number': '',
				'Classification': '',
				'Auction Description': '',
				'Amazon Description': ''
			}
			product['Inventory Number'] = row['Inventory Number']
			product['Classification'] = row['Classification']
			product['Auction Description'] = row['Auction Description']

			soup = BS(row['Auction Description'], "html.parser")
			tablerows = soup.find_all('tr')
			empty_val = False
			if len(tablerows) > 2:
				product['Amazon Description'] = ''
			else:
				headings = []
				values = []				
				string = '<p><b>Summary</b></p><br></br>'
				for heading in soup.find_all('th'):
					headings.append(heading.text.strip())

				for data in soup.find_all('td'):
					if data.text == '':
						empty_val = True					
					values.append(data.text.strip())

				count = 0
				print(headings)
				print(row['Inventory Number'])

				if empty_val:
					product['Amazon Description'] = ''
				elif len(headings) == len(values):
					for value in headings:
						string += '<p><b>- {}</b>: {}</p>'.format(value, values[count])
						count += 1
					product['Amazon Description'] = string
				else:
					product['Amazon Description'] = ''
				
				
				print(string)
				
			rows.append(product)

	
	saveProductsToCsv(rows)


def saveProductsToCsv(products):
	if not os.path.exists('./output'):
		os.makedirs('./output')

	file_count = 0
	helper.progress(file_count, len(products), "Creating File", "Complete")
	rows = []
	file_name = './output/AmazonDescriptions.csv'
	for product in products:        
		# find file headings
		headings = helper.findCategoryHeadings(product)
		row = {}
		# add products by row
		for key, value in product.items():
			# sort product details for file insertion            
			row[key] = value

		rows.append(row)

	try:
		# create and write to file based on category
		with open(file_name, "w", newline="", encoding='utf-8') as f:
			writer = csv.DictWriter(f, headings)
			writer.writeheader()
			writer.writerows(rows)
		file_count = file_count + 1
		helper.progress(file_count, len(products), "Creating File", "Complete")
	except PermissionError as e:
		print(e)
		output = False
	except UnicodeEncodeError as e:
		print(e)
		output = False
	else:
		output = True
	print('')

	return output



if __name__ == "__main__":
    app()
