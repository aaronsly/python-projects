from bs4 import BeautifulSoup as BS
import json, csv, random, helper, connection, database, re, time, os, requests
from time import sleep
from crawler import Crawler

def create_soup(html):
    return BS(html, "html.parser")

def scrape():
    urls = get_urls()   
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    products = []
    url_counter = 0
    print("Total Urls: {}".format(len(urls)))
    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
    for url in urls:

        invalid = False
        for string in ['.pdf', '#', 'tel:', '{{']:
            if string in url:
                invalid = True
                break            

        if invalid:
            print()
            print("Invalid Url: {}".format(url))
            continue
        try:
            request = session.get(url, headers={'user-agent': ua})

            if 200 != request.status_code:
                continue

            product = {
                'name': '',
                'product_code': '',
                'url': url,
                'description_list': [],
                'specs': {},
                'meta' : {}
            }
            conf_products = []

            page = create_soup(request.content)
            product_html = page.select_one('.catalog-product-view')
            
            if product_html:
                product_name = product_html.select_one('.product-name')
                product_code = product_html.select_one('.product-ref span')
                description = product_html.select_one('.tab-content .std')
                specs = product_html.select_one('#product-attribute-specs-table')
                
                product['name'] = product_name.text.strip() if product_name else "No Product Name"
                product['product_code'] = product_code.text.strip() if product_code else "No Product Code"                    

                if description:                    
                    for list_item in description.findAll('li'):                    
                        product['description_list'].append(list_item.text)

                meta_description = page.find("meta", {"name": "description"})
                #print(description)
                keywords = page.find("meta", {"name": "keywords"})
                #print(keywords)
                if meta_description:
                    product['meta']['description'] = meta_description['content']
                if keywords:
                    product['meta']['keywords'] = keywords['content']
                if page.select_one('title'):
                    product['meta']['title'] = page.select_one('title').text.strip()

                if specs:
                    # if config product
                    if product_html.select_one('.super-attribute-select'):                        
                        for script in page.find_all('script'):
                            s = script.text
                            if "var spConfig = new Product.Config" in s:
                                start = 'Product.Config('
                                end = ');'
                                config = s[s.find(start)+len(start):s.rfind(end)]
                                json_data = json.loads(config)                               
                                
                                for k, v in json_data['childProducts'].items():
                                    
                                    conf_product = {
                                        'name': v['productName'],
                                        'product_code': v['sku'],
                                        'url': url,
                                        'description_list': [],
                                        'specs': {},
                                        'meta': product['meta']
                                    }

                                    conf_description = create_soup(v['description'])
                                    for list_item in conf_description.findAll('li'):
                                        conf_product['description_list'].append(list_item.text)

                                    attrs = create_soup(v['productAttributes'])
                                    for row in attrs.find_all("tr"):
                                        label = row.select_one(".label").text.strip().lower()
                                        value = row.select_one(".data").text.strip()

                                        if value.isdigit() and label != 'brochure page:':
                                            value = float(value)

                                        if label == "product reference":
                                            continue

                                        conf_product['specs'][label.replace(":", "")] = value

                                    conf_products.append(conf_product)

                    else:
                        
                        for row in specs.find_all("tr"):
                            label = row.select_one(".label").text.strip().lower()
                            value = row.select_one(".data").text.strip()

                            if value.isdigit() and label != 'brochure page:':
                                value = float(value)

                            if label == "product reference":
                                continue

                            product['specs'][label.replace(":","")] = value                

                #print()
                #print(product)                
                products.append(product)
                if conf_products:                    
                    products = products + conf_products

        except Exception as e:
            print()
            print("ERROR!!!") 
            print(e)     

        url_counter += 1
        helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

    saveProductsToCsv(products)
    

def get_urls():
    file_name = './urls/frontline_sitemap_limit.json'
    # Open and return urls if file exists
    if os.path.isfile(file_name):
        with open(file_name) as f:
            return json.load(f)


def limit_urls():
    file_name = './urls/frontline_sitemap.json'
    sorted_links = []
    if os.path.isfile(file_name):
        with open(file_name) as f:
            links = json.load(f)

    for link in links:
        invalid = False
        remove = ['.pdf',
        '#',
        'tel:',
        'website',
        '#',
        'add/product',
        'review/product/view',
        'inspiration-hub',
        '?cat',
        '?bath_length',
        '.xml',
        '?dir',
        '?manufacturer',
        '?material',
        '?brand',
        '?size',
        '?width',
        '?style',
        '?price',
        '?inside_depth',
        '?p',
        '?projection',
        '?back_depth',
        '?basin_shape',
        '?handing',
        '?back_type',
        '?q',
        '?holes']

        for string in remove:
            if string in link:
                invalid = True
                break

        if invalid:       
            continue             
        
        sorted_links.append(link)

    # print(sorted_links)
    with open('./urls/frontline_sitemap_limit.json', 'w') as outfile:
        json.dump(sorted_links, outfile)


def build_sitemap(url):
    # initializeing crawler
    crawler = Crawler(url)
    # fetch links
    links = crawler.start()
    # Save urls to json file
    with open('./urls/frontline_sitemap_pyton.json', 'w') as outfile:
        json.dump(links, outfile)


def saveProductsToCsv(products):
    if not os.path.exists('./output'):
        os.makedirs('./output')
    
    file_count = 0    
    helper.progress(file_count, len(products), "Creating File", "Complete")
    rows = []
    for product in products:
        file_name = './output/frontline-products-for-amber-dines.csv'
        # find file headings
        headings = findCategoryHeadings(product)        
        row = {}
        # add products by row
        for key, value in product.items():            
            # sort product details for file insertion
            if key == 'description_list':
                row[key] = '\n'.join(value)             

                # AMAZON FEATURES & BENEFITS AND EBAY BULLET POINTS
                am_feat = '<p><b>Features and Benefits</b></p></br></br>\n'
                ebay = '<ul>\n'

                for feature in value:
                   am_feat += "<p><b>-{}</b></p>\n".format(feature.capitalize())
                   ebay += "<li>{}</li>\n".format(feature.capitalize())
                
                ebay += '</ul>'

                row['amazon_description'] = am_feat
                row['ebay_description'] = ebay

            elif key == 'specs' and value:
                string = ''
                for k, v in value.items():
                    string += '{}: {}\n'.format(k,v)
                row[key] = string

                # AMAZON AND EBAY SPECS
                am_specs = '<p><b>Summary</b></p><br></br>\n'
                ebay_specs = '<table>\n<tr>\n'
                table_headers = ''
                table_cells = ''

                for k, v in value.items():
                    if k not in ['brochure code','brochure page']:
                        label = k.capitalize()
                        if isinstance(v, (int, float)):
                            data = v
                        else:
                            data = v.capitalize()

                        am_specs += "<p><b>-{}: </b>{}</p>\n".format( label, data)
                        table_headers += "<th>{}</th>\n".format(label)
                        table_cells += "<td>{}</td>\n".format(data)                

                row['ebay_specs'] = ebay_specs + table_headers + "</tr>\n<tr>\n" + table_cells +"</tr>\n</table>"
                row['amazon_specs'] = am_specs

            elif key == 'meta':
                string = ''
                for k, v in value.items():
                    string += '{}: {}\n'.format(k, v)
                row[key] = string
            else:
                row[key] = value
        
        rows.append(row)

    try:
        # create and write to file based on category
        with open(file_name, "w", newline="", encoding='utf-8') as f:
            writer = csv.DictWriter(f, headings)
            writer.writeheader()
            writer.writerows(rows)
        file_count = file_count + 1
        helper.progress(file_count, len(products),"Creating File", "Complete")
    except PermissionError as e:
        print(e)
        output = False
    except UnicodeEncodeError as e:
        print(e)
        output = False
    else:
        output = True
    print('')
    return output


def findCategoryHeadings(product):    
    headings = []
    # find headings
    for key in product.keys():
        if key not in headings:
            headings.append(key)
            if key == 'description_list':
                headings = headings + ['amazon_description', 'ebay_description']
            if key == 'specs':
                headings = headings + ['amazon_specs', 'ebay_specs']
    return headings
