from bs4 import BeautifulSoup as BS
import json
import csv
import random
import helper
import connection
import database
import re
import time
import os
import requests
from time import sleep

from datetime import datetime, timedelta


def create_soup(html):
    return BS(html, "html.parser")


def scrape():
    urls = get_urls()
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    products = []
    errors = []
    url_counter = 0
    print("Total Urls: {}".format(len(urls)))
    helper.progress(url_counter, len(urls), "Scraping Product Data", "Complete")

    for url in urls:

        try:
            request = session.get(url, headers={'user-agent': ua})
            # print()
            # print(request.status_code)
            # print(url)
            
            if 200 != request.status_code:
                continue

            product = {
                'name': '',
                'product_code': '',
                'url': url,
                'description': '',
                'specs': {},
            }
            # conf_products = []

            page = create_soup(request.content)
            product_html = page.select_one('.product-template-default')

            if product_html:
                product_name = product_html.select_one('h1')
                product_code = product_html.select_one('.modus-block-left .ct-product-right')
                description = product_html.select_one('#tab-description')
                specs = product_html.select_one('#tab-additional_information')

              
                product['name'] = product_name.text.strip() if product_name else "No Product Name, sorry :("
                product['product_code'] = product_code.find(text=True, recursive=False).strip().replace('Product Code: ', "") if product_code else "No Product Code, sorry :("
                product['description'] = description.text.replace('Description', "").strip() if description else "No description, sorry :("

                # meta_description = page.find("meta", {"name": "description"})
                # #print(description)
                # keywords = page.find("meta", {"name": "keywords"})
                # #print(keywords)
                # if meta_description:
                #     product['meta']['description'] = meta_description['content']
                # if keywords:
                #     product['meta']['keywords'] = keywords['content']
                # if page.select_one('title'):
                #     product['meta']['title'] = page.select_one(
                #         'title').text.strip()

                if specs:
                    for row in specs.find_all("tr"):
                        label = row.select_one("th").text.strip().lower()
                        value = row.select_one("td").text.strip()

                        if value.isdigit():
                            value = float(value)
                        if value:
                            product['specs'][label] = value                
                products.append(product)
            else:
                errors.append("Not a product page: {}".format(url))
        except Exception as e:
            print()
            print("ERROR!!!")
            print(e)

        url_counter += 1
        helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

    saveProductsToCsv(products)

def create_urls():
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    urls = []
    request = session.get('https://www.cassellie.co.uk/sitemap_index.xml', headers={'user-agent': ua})

    if 200 != request.status_code:
        return False

    sitemaps = create_soup(request.content)
    for sitemap in sitemaps.find_all('loc'):

        if 'product-sitemap' in sitemap.text:
            product_request = session.get(
                sitemap.text, headers={'user-agent': ua})

            if 200 != product_request.status_code:
                return False

            product_urls = create_soup(product_request.content)

            for url in product_urls.find_all('loc'):
                urls.append(url.text)
        else:
            continue

    with open('./urls/cassellie_product_urls.json', 'w') as outfile:
        json.dump(urls, outfile)

    return urls


def get_urls():

    file_name = './urls/cassellie_product_urls.json'
    if os.path.isfile(file_name):

        file_mod_time = datetime.fromtimestamp(os.stat(file_name).st_mtime)

        if datetime.today() - file_mod_time > timedelta(days=1):
            print("Recreating Urls file")
            return create_urls()

        else:
            with open(file_name) as f:
                links = json.load(f)
            return links
    else:
        return create_urls()      
    


def saveProductsToCsv(products):
    if not os.path.exists('./output'):
        os.makedirs('./output')

    file_count = 0
    helper.progress(file_count, len(products), "Creating File", "Complete")
    rows = []
    for product in products:
        file_name = './output/cassellie-products.csv'
        # find file headings
        headings = findCategoryHeadings(product)
        row = {}
        # add products by row
        for key, value in product.items():
            # sort product details for file insertion
            
            if key == 'specs' and value:
                string = ''
                for k, v in value.items():
                    if k in ['weight', 'dimensions','range']:
                        row[k] = v
                    else:
                        string += '{}: {}\n'.format(k, v)
                row[key] = string            
            else:
                row[key] = value

        rows.append(row)

    try:
        # create and write to file based on category
        with open(file_name, "w", newline="", encoding='utf-8') as f:
            writer = csv.DictWriter(f, headings)
            writer.writeheader()
            writer.writerows(rows)
        file_count = file_count + 1
        helper.progress(file_count, len(products), "Creating File", "Complete")
    except PermissionError as e:
        print(e)
        output = False
    except UnicodeEncodeError as e:
        print(e)
        output = False
    else:
        output = True
    print('')
    return output


def findCategoryHeadings(product):
    headings = []
    # find headings
    for key in product.keys():
        if key not in headings:                        
            headings.append(key) 
            if key == 'url':
                headings.append('weight')
                headings.append('dimensions')
                headings.append('range')
    return headings
