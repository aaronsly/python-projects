import connection
import helper
import scrape

#tor.exe -f ../Data/Tor/torrc | more

def app():
    helper.clear()
    if connection.check_tor_connection():
        # scrape.best_heating()
        #print(scrape.soak())
        #print(scrape.trade_radiators())
        print(scrape.heat_and_plumb())
        #connection.kill_tor()
        # Sites to scrape
        # https://www.traderadiators.com/sitemap.xml
        # https://soak.com/en-gb/sitemap_0-product.xml
        # https://www.heatandplumb.com/sitemap/google-sitemap-index.xml
        # https://www.tradeplumbing.co.uk/catalog/seo_sitemap/product/


if __name__ == "__main__":
    app()
