from bs4 import BeautifulSoup as BS
import json, random, helper, connection, database, re, time, os, requests
from time import sleep

def create_soup(html):
    return BS(html, "html.parser")


def best_heating():
    # https://www.bestheating.com/sitemap.xml
    base_url = 'https://www.bestheating.com'
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    request = session.get(base_url + "/sitemap.xml", headers={'user-agent': ua})

    if 200 != request.status_code:
        return False

    urls = create_soup(request.content).findAll('url')

    if not urls:
        return False

    url_counter = 0
    print("Total Urls: {}".format(len(urls)))
    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

    for url in urls:
        loc = url.find('loc').text
        priority = float(url.find('priority').text)
        product = {
            'url': loc,
            'height': 0,
            'width': 0,
            'depth': 0,
            'brand': '',
            'material': '',
            'colour': '',
            'price': 0,
            'images':[],
            'meta': {},
            'related_products': [],
            'documents':[],
        }

        if priority == 0.8:
            if url_counter >= 0:  # testing only
                # Reset ip and user agent to avoid being blocked
                if url_counter % random.choice([3, 9, 25, 50, 100, 250]) == 0 and url_counter != 0:
                    ua = connection.get_user_agent()
                    connection.renew_tor_connection()
                    session = connection.get_tor_session()
                    print()
                    print("Switching IP")
                    sleep(2)
                try:
                    prod_page = session.get(loc, headers={'user-agent': ua})
                    product_html = create_soup(prod_page.content)
                    product_name = product_html.select_one('.product-name > h1')
                    product_code = product_html.select_one('.sku > span')
                    specs_table = product_html.select_one('#product-attribute-specs-table')
                    image_container = product_html.select_one('#media-image')
                    rel_prod_container = product_html.select_one('.related-products')
                    guides = product_html.select_one('.pdf-list')
                    price = product_html.find("meta", {"itemprop": "price"})
                    product['price'] = float(price.attrs['content']) if price else "Not Found"                    
                    product['name'] = product_name.text.strip() if product_name else "No Product Name"
                    product['product_code'] = product_code.text.strip() if product_code else "No Product Code"                                         

                    if specs_table:
                        for row in specs_table.find_all("tr"):
                            label = row.select_one(".label").text.strip().lower()
                            value = row.find(attrs={'class': 'data'}).text.strip()

                            if value.isdigit():
                                value = float(value)

                            if "dimensions" in label:
                                value = value.lower()
                                # remove unwanted characters to avoid issues when splitting
                                for c in ["h","w","d"," ", "xx"]:
                                    if c == "xx":
                                        value = value.replace("xx", "x")
                                        continue
                                    if c in value:
                                        value = value.replace(c,"") 
                                
                                split_val = value.split("x")
                                
                                if len(split_val) > 1:
                                    product["height"] = float(helper.str_to_int(split_val[0]))
                                    product["width"] = float(helper.str_to_int(split_val[1]))                                    
                                else:
                                    product["width"] = value
                                if len(split_val) > 2:
                                    product["depth"] = float(helper.str_to_int(split_val[2])) if split_val[2] else 0
                                continue

                            product[label] = value
                    # find product category
                    for script in product_html.findAll("script"):
                        s = script.text
                        if "ecomm_prodcategory" in s:                            
                            start = 'ecomm_prodcategory: ["'
                            end = '"],'
                            product['category'] = s[s.find(start)+len(start):s.rfind(end)]

                    if 'category' not in product:
                        product['category'] = 'Uncategorised'
                    
                    # grab url of each product image
                    if image_container:
                        imgs = image_container.find_all('a')
                        for img in imgs:
                            product['images'].append(img['href'])

                    # get related product info
                    if rel_prod_container:
                        for prod in rel_prod_container.find_all('li'):
                            name = prod.select_one('.product-name').text.strip()
                            price = prod.select_one('.price').text.strip()
                            img = prod.find('img')
                            link = prod.find('a', {'class': 'product-image'})
                            
                            product['related_products'].append({
                                'name': name,
                                'price': helper.price_str_to_int(price),
                                'image': img['src'],
                                'link' : link['href']
                            })

                    # get meta data for seo
                    # keywords, description title tag etc
                    description = product_html.find("meta", {"name": "description"})
                    keywords = product_html.find("meta", {"name": "keywords"})
                    if description:
                        product['meta']['description'] = description['content']
                    if keywords:
                        product['meta']['keywords'] = keywords['content']
                    if product_html.select_one('title'):
                        product['meta']['title'] = product_html.select_one('title').text.strip()

                    if guides:
                        for guide in guides.find_all('a'):
                            product['documents'].append(base_url + guide['href'])

                    url_counter += 1
                    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
                except AttributeError as e:
                    print(e, loc)
                    print()
                
                # Write product infomation to database
                database.write_product("best_heating_products", product)
            else:
                url_counter += 1
                helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
        else:
            url_counter += 1
            helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
    return True

# def trade_radiators():
#     url = "https://www.traderadiators.com/sitemap.xml"
#     session = connection.get_proxy_session(url)    
#     request = session.get(url)
    
#     if 200 != request.status_code:
#         print(request.content)
#         return False

#     urls = create_soup(request.content).findAll('url')

#     if not urls:
#         return False

#     url_counter = 0
#     print("Total Urls: {}".format(len(urls)))
#     #helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
#     ignore = database.get_table_contents('trade_ignore_urls')
#     print(ignore)

#     for url in urls:
#         loc = url.find('loc').text

#         if helper.search_dict_list(loc,ignore,'url'):            
#             #print("is in ignore: {}".format(loc))
#             continue

#         if loc not in ignore:
#             priority = float(url.find('priority').text)
#             product = {
#                 'url': loc,
#                 'height': 0,
#                 'width': 0,
#                 'depth': 0,
#                 'material': '',
#                 'colour': '',
#                 'price': 0,
#                 'meta': {}
#             }

#             if priority == 1.00:
#                 if url_counter < 10:  # testing only
#                     print(loc)
#                     # Reset ip and user agent to avoid being blocked
#                     if url_counter % random.choice([50, 100, 250]) == 0 and url_counter != 0:                        
#                         session = connection.get_proxy_session(url)
                    
#                     try:
#                         prod_page = session.get(loc)
#                         product_html = create_soup(prod_page.content)                    
#                         if product_html.select_one('#product-page-body'):                                    
#                             product_name = product_html.select_one('#title-box > h1')
#                             product_code = product_html.select_one('#title-box > .product-ref > span')
#                             product['brand'] = product_html.select_one('#title-box > .product-ref > span > span').text
#                             specs_table = product_html.find('div', {'class': ['resp-tab-content-active','info']})
#                             product['product_code'] = product_code.text.strip() if product_code else "No Product Code"
#                             product['name'] = product_name.text.strip() if product_name else "No Product Name"

#                             # Get price or starting price for config product
#                             if product_html.select_one('.attribute-list'):
#                                 if product_code:
#                                     code = product_code.text.strip()
#                                     price = product_html.select_one('#id'+code+'StaticPrice').text.split(" Including")[0]
#                                     product['price'] = "From " + price.replace(" Price: ", "")
#                             else:                                
#                                 price = product_html.select_one("span.product-price > span")
#                                 product['price'] = float(price.text.strip()) 

#                             if specs_table:                               
#                                 specs = {}
#                                 parts = str(specs_table.select_one('p')).split("<br/>")
#                                 # Create specs Dict
#                                 for p in parts:
#                                     attr = helper.remove_html_tags(p).split(":")
#                                     value = attr[1].strip().replace("mm", "")

#                                     if value.isdigit():
#                                         value = float(value)

#                                     specs[attr[0]] = value                               
                                
#                                 product['height'] = specs['Height'] 
#                                 product['width'] = specs['Width']

#                                 try:
#                                     product['material'] = specs['Material']
#                                 except KeyError:
#                                     product['material'] = "N/A"
#                                 try:                                
#                                     product['colour'] = specs['Finish']
#                                 except:
#                                     product['colour'] = "N/A"

#                             print(product)
#                             url_counter += 1
#                             #helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

#                             # Write product infomation to database
#                             #database.write_product("trade_radiators_products", product)
#                         else:
#                             url_counter += 1
#                             #helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
#                             database.write_ignore_url('trade_ignore_urls',loc)
#                             continue
#                     except AttributeError as e:
#                         print(e, loc)
#                         print()                    

#                     # Write product infomation to database
#                     # database.write_product("trade_radiators_products", product)
#         else:
#             continue
#     return True

def soak():
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    base_url = 'https://soak.com'
    request = session.get(base_url + "/en-gb/sitemap_0-product.xml", headers={'user-agent': ua})    

    if 200 != request.status_code:
        return False

    urls = create_soup(request.content).findAll('url')

    if not urls:
        return False

    url_counter = 0
    print(base_url + " Total Urls: {}".format(len(urls)))
    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

    for url in urls:
        if url_counter > 1060:
            loc = url.find('loc').text
            product = {
                'url': loc,
                'height': 0,
                'width': 0,
                'depth': 0,
                'material': '',
                'colour': '',
                'price': 0,
                'meta': {}
            }
        
            # Reset ip and user agent to avoid being blocked
            if url_counter % random.choice([3,9,25,50,100, 250]) == 0 and url_counter != 0:
                ua = connection.get_user_agent()
                connection.renew_tor_connection()
                session = connection.get_tor_session()
                print()
                print("Switching IP")
                sleep(2)

            try:
                prod_page = session.get(loc, headers={'user-agent': ua})
            except Exception as e:
                print()
                print(e)
                ua = connection.get_user_agent()
                connection.renew_tor_connection()
                session = connection.get_tor_session()
                prod_page = session.get(loc, headers={'user-agent': ua})

            if 200 != prod_page.status_code:                            
                if prod_page.status_code == 404:
                    print()
                    print(loc + " IS 404")
                    # add check for url in db and delete if needed
                    continue
                else:
                    print()
                    print(loc + " IS " + prod_page.status_code)
                    continue
            
            try:               
                product_html = create_soup(prod_page.content)
                product_name = product_html.select_one('title')
                specs = product_html.select_one('.product-tabs__specifications-content .product-main-attributes ul')
                price = product_html.find_all('div', {'class': 'pdetails__actions__pricing'})
                additional_info = product_html.findAll('div', {'class': 'pdp-desc__incex__content'})
                product['name'] = product_name.text.strip() if product_name else "No Product Name"

                if price:
                    for item in price:
                        product['price'] = float(item.attrs['data-current-value'])
                else:
                    product['price'] = 0

                if specs:
                    for row in specs.find_all("li"):
                        label = row.select_one(".label").text.strip().lower()
                        value = row.select_one(".value").text.strip().replace(" mm", "")

                        # Assign finish to colour
                        if label == 'finish':
                            product['colour'] = value
                            continue 
                        # assisgn sku to product_code column
                        if label == 'sku':
                            product['product_code'] = value
                            continue

                        if value.isdigit():
                            value = float(value)

                        product[label] = value

                # add delivery cost to price if under free shipping cut off of 399
                if isinstance(product['price'], (int, float)) and product['price'] <= 399:

                    group = product_html.select_one('.shipping-strategy__asset-group')

                    if group:
                        standard_del = group.find(text=re.compile("Standard Delivery"))
                        nom_day = group.find(text=re.compile("Select the day"))

                        if standard_del:
                            delivery = standard_del.parent
                        elif nom_day:
                            delivery = nom_day.parent

                        del_price = float(delivery.select_one('.shipping-method-price').text.strip().replace("£",""))

                    else:
                        del_price = 9.99

                    product['delivery_price'] = del_price
                    product['price'] = float("{0:.2f}".format(product['price'] + del_price))

                # Assign length to width to save to db if no width attr on page
                if product['width'] == 0 and 'length' in product:
                    product['width'] = product['length']
                    del product['length']

                if 'brand' not in product:
                    try:
                        product['brand'] = product['range']
                        del product['range']
                    except KeyError as e:                        
                        product['brand'] = 'Soak'
                        sleep(5)
                        

                if 'category' not in product:
                    breadcrumbs = product_html.select_one(".breadcrumbs__nav").find_all("li")
                    for count, crumb in enumerate(breadcrumbs, start=1):                        
                        if count == len(breadcrumbs) - 1:                           
                            product['category'] = crumb.select_one('a').text.strip()

                # Get what's included and excluded with the product
                if additional_info:
                    product['includes'] = []
                    product['excludes'] = []
                    for info in additional_info:
                        for item in info.find_all('li'):
                            label = item.select_one(".label").text.strip()
                            value = item.select_one(".value").text.strip()
                            
                            if "Includes" in info.text:
                                if not value:
                                    product['includes'].append({"item": label})
                                else:
                                    link = item.select_one(".value a")
                                    product['includes'].append({
                                        "item": label,
                                        "link": base_url + link['href'],
                                        "product_code": link['title']
                                    })                                    
                            elif "Excludes" in info.text:
                                if not value:
                                    product['excludes'].append({"item": label})
                                else:
                                    link = item.select_one(".value a")
                                    product['excludes'].append({
                                        "item": label,
                                        "link": base_url + link['href'],
                                        "product_code": link['title']
                                    })
                # get meta data for seo
                # keywords, description title tag etc
                description = product_html.find("meta", {"name": "description"})
                keywords = product_html.find("meta", {"name": "keywords"})
                if description:
                    product['meta']['description'] = description['content']
                if keywords:
                    product['meta']['keywords'] = keywords['content']
                if product_html.select_one('title'):
                    product['meta']['title'] = product_html.select_one('title').text.strip()
                
                url_counter += 1
                helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
            except (AttributeError, ValueError, KeyError, TypeError) as e:
                print(e, loc)
                print()
                sleep(5)
            
            # Write product infomation to database
            database.write_product("soak_products", product)
            sleep(1)
        else :
            url_counter += 1
            helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
    return True

def heat_and_plumb():
    base_url = 'https://www.heatandplumb.com'
    sitemap = base_url + "/sitemap/google-sitemap-index.xml"
    ignore_urls = ['https://www.heatandplumb.com/sitemap/google-sitemap-sections.xml',
                   'https://www.heatandplumb.com/sitemap/google-sitemap-pages.xml',
                   'https://www.heatandplumb.com/sitemap/google-sitemap-images-products.xml',
                   'https://www.heatandplumb.com/sitemap/google-sitemap-products-accessories.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-basins.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bath-panels.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bath-screens.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bathroom-accessories.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bathroom-cabinets.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bathroom-furniture.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bathroom-mirrors.xml',
                #    'https://www.heatandplumb.com/sitemap/google-sitemap-products-bathroom-suites.xml'
                   ]
    session = connection.get_proxy_session(sitemap)

    # Scrape urls for product category sitemaps
    cat_request = session.get(sitemap)
    if 200 != cat_request.status_code:
        return False

    cat_urls = create_soup(cat_request.content).findAll('sitemap')
   
    if not cat_urls:
        return False
    
    # Loop over category sitemap urls and scrape sub category sitemap urls
    for cat_url in cat_urls:
        cat_loc = cat_url.find('loc').text

        if cat_loc in ignore_urls:
            continue

        request = session.get(cat_loc, timeout=60)

        if 200 != request.status_code:
            return False

        urls = create_soup(request.content).findAll('url')

        if not urls:
            return False

        url_counter = 0
        print(cat_loc + " Total Urls: {}".format(len(urls)))
        helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

        # scrape each product in the category
        for url in urls:
            if url_counter >= 1140:
                loc = url.find('loc').text
                product = {
                    'url': loc,
                    'height': 0,
                    'width': 0,
                    'depth': 0,
                    'material': '',
                    'colour': '',
                    'price': 0,
                    'images': [],
                    'meta': {},
                    'related_products': [],
                    'documents': [],
                }

                # Reset ip and user agent to avoid being blocked
                if url_counter % random.choice([9, 25, 50, 100, 250]) == 0 and url_counter != 0:
                    print()
                    print("Switching IP")
                    session = connection.get_proxy_session(loc)
                    sleep(2)

                # Grab page content
                try:
                    prod_page = session.get(loc, timeout=90)
                except ConnectionError as e:
                    print()
                    print(e)
                    session = connection.get_proxy_session(loc)
                    prod_page = session.get(loc, timeout=90)

                if 200 != prod_page.status_code:
                    if prod_page.status_code == 404:
                        print()
                        print(loc + " IS 404")
                        # add check for url in db and delete if needed
                        continue
                    else:
                        print()
                        print(loc + " IS " + prod_page.status_code)
                        continue
                # Extract Content
                try:
                    product_html = create_soup(prod_page.content)
                    product_name = product_html.select_one('.product_detail_right h1')
                    specs = product_html.select_one('#tab1 .data-table')
                    price = product_html.select_one('.product_detail_price .price_main')
                    modal_detail = product_html.select_one('.modal_prod_detail ul')                    

                    if product_name:
                        text = product_name.text.strip().split("Reference: ")
                        product['name'] = text[0]
                        product['product_code'] = text[1]
                    else:
                        product['name'] = "No Product Name"
                        product['product_code'] = "No Product Code"

                    product['price'] = helper.price_str_to_int(price.text.strip()) if price else 0

                    # Get products specs and extra detail
                    if specs:
                        for row in specs.find_all("tr"):
                            if row.select_one(".label"):
                                label = row.select_one(".label").text.strip().lower()
                            else:
                                continue
                            if row.select_one(".data"):
                                value = row.select_one(".data").text.strip().replace("mm", "")
                            else:
                                continue

                            # Assign finish to colour
                            if label == 'finish':
                                product['colour'] = value
                                continue

                            if label in ["width","height","length"]:
                                if "-" in value:
                                    split = value.replace(" ","").split("-")
                                    product[label] = max(split) 
                                    continue             

                            if value.isdigit() and label != 'ean barcode':
                                value = float(value)

                            product[label] = value

                    if modal_detail:          
                        for li in modal_detail.find_all("li"):
                            values = li.text.strip().split(":")                        
                            if values[0] != "Reference":
                                key = values[0].strip().lower()
                                val = values[1].strip()
                                product[key] = val

                    if 'category' not in product:
                        breadcrumbs = product_html.select_one(".breadcrumb").find_all("li")
                        for count, crumb in enumerate(breadcrumbs, start=1):
                            if count == len(breadcrumbs) - 1:
                                product['category'] = crumb.select_one('a').text.strip()

                    # get meta data for seo
                    # keywords, description title tag etc
                    description = product_html.find("meta", {"name": "description"})
                    keywords = product_html.find("meta", {"name": "keywords"})
                    if description:
                        product['meta']['description'] = description['content']
                    if keywords:
                        product['meta']['keywords'] = keywords['content']
                    if product_html.select_one('title'):
                        product['meta']['title'] = product_html.select_one('title').text.strip()

                    # grab url of each product image
                    image_container = product_html.select_one('.slider_full_width')
                    if image_container:
                        imgs = image_container.find_all('img')
                        for img in imgs:
                            if img['src'] not in product['images']:
                                product['images'].append(img['src'])

                    # get product documents
                    docs = product_html.select_one('.product_documents_detail')
                    if docs:
                        for doc in docs.find_all('a'):
                            product['documents'].append(base_url + doc['href'])
                    # get technical drawings
                    for item in product_html.find_all("div", attrs={'data-title' : True}):
                        if "Product Dimensions" in item['data-title']:
                            for img in item.find_all('img'):
                                product['documents'].append(img['src'])

                    # get related products
                    may_need_products = product_html.select_one('.you-may-need-block')                    
                    if may_need_products:    
                        for prod in may_need_products.find_all('li'):
                            prod_name = prod.select_one('.may_need_desc p a')
                            if not prod_name:
                                prod_name = prod.select_one('.may_need_desc p')

                            rel_product = {
                                'product_code': helper.replace_all(["(",")"], "", prod.select_one('.need_prod_ref').text.strip()),
                                'price': helper.price_str_to_int(prod.select_one('.prod_checkbox_price').text.strip()),
                                'name': ''.join(prod_name.find_all(text=True, recursive=False)).strip(),
                            }

                            for img in prod.find_all('img'):
                                rel_product['image'] = img['src']
                            
                            if rel_product['product_code'] != product['product_code']: 
                                product['related_products'].append(rel_product)

                    may_like = product_html.select_one('.you_may_also_like')                    
                    if may_like:                        
                        for like in may_like.find_all('div', {'class': 'like_prod_item'}):
                            name = like.select_one('.like_prod_name')
                            rel_product = {
                                'price': helper.price_str_to_int(like.select_one('.like_view_price').text.strip()),
                                'name': name.text.strip(),
                            }
                            imgs = like.find_all('img')
                            if imgs:                                
                                for img in imgs:
                                    rel_product['image'] = img['src']
                            
                            for link in name.find_all('a'):
                                rel_product['link'] = link['href']                            
                            product['related_products'].append(rel_product)

                    url_counter += 1
                    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
                except (AttributeError, ValueError, KeyError, TypeError) as e:
                    print()
                    print(e)
                    print(loc)
                    print(product)
                    print()
                    sleep(5)
                
                # print(product)                
                # Write product infomation to database                
                database.write_product("heat_plumb_products", product) 
            else:
                url_counter += 1
                helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
        return True # remove on live
    return True

def heat_and_plumb2():    
    cat_urls = ['https://www.heatandplumb.com/sitemap/google-sitemap-products-radiators.xml']
    session = connection.get_proxy_session(cat_urls[0])
    base_url = 'https://www.heatandplumb.com'
    # Loop over category sitemap urls and scrape sub category sitemap urls
    for cat_url in cat_urls:        
        request = session.get(cat_url, timeout=60)

        if 200 != request.status_code:
            return False

        urls = create_soup(request.content).findAll('url')

        if not urls:
            return False

        url_counter = 0
        print(cat_url + " Total Urls: {}".format(len(urls)))
        helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

        # scrape each product in the category
        for url in urls:
            if url_counter >= 6290:
                loc = url.find('loc').text
                product = {
                    'url': loc,
                    'height': 0,
                    'width': 0,
                    'depth': 0,
                    'material': '',
                    'colour': '',
                    'price': 0,
                    'images': [],
                    'meta': {},
                    'related_products': [],
                    'documents': [],
                }

                # Reset ip and user agent to avoid being blocked
                if url_counter % random.choice([9, 25, 50, 100, 250]) == 0 and url_counter != 0:
                    print()
                    print("Switching IP")
                    session = connection.get_proxy_session(loc)
                    sleep(2)

                # Grab page content
                try:
                    prod_page = session.get(loc, timeout=90)
                except (requests.exceptions.RequestException) as e:
                    print()
                    print(e)
                    session = connection.get_proxy_session(loc)
                    prod_page = session.get(loc, timeout=90)

                if 200 != prod_page.status_code:
                    if prod_page.status_code == 404:
                        print()
                        print(loc + " IS 404")
                        # add check for url in db and delete if needed
                        continue
                    else:
                        print()
                        print(loc + " IS " + prod_page.status_code)
                        continue
                # Extract Content
                try:
                    product_html = create_soup(prod_page.content)
                    product_name = product_html.select_one('.product_detail_right h1')
                    specs = product_html.select_one('#tab1 .data-table')
                    price = product_html.select_one('.product_detail_price .price_main')
                    modal_detail = product_html.select_one('.modal_prod_detail ul')                    

                    if product_name:
                        text = product_name.text.strip().split("Reference: ")
                        product['name'] = text[0]
                        product['product_code'] = text[1]
                    else:
                        product['name'] = "No Product Name"
                        product['product_code'] = "No Product Code"

                    product['price'] = helper.price_str_to_int(price.text.strip()) if price else 0

                    # Get products specs and extra detail
                    if specs:
                        for row in specs.find_all("tr"):
                            if row.select_one(".label"):
                                label = row.select_one(".label").text.strip().lower()
                            else:
                                continue
                            if row.select_one(".data"):
                                value = row.select_one(".data").text.strip().replace("mm", "")
                            else:
                                continue

                            # Assign finish to colour
                            if label == 'finish':
                                product['colour'] = value
                                continue

                            if label in ["width","height","length"]:
                                if "-" in value:
                                    split = value.replace(" ","").split("-")
                                    product[label] = max(split) 
                                    continue             

                            if value.isdigit() and label != 'ean barcode':
                                value = float(value)

                            product[label] = value

                    if modal_detail:          
                        for li in modal_detail.find_all("li"):
                            values = li.text.strip().split(":")                        
                            if values[0] != "Reference":
                                key = values[0].strip().lower()
                                val = values[1].strip()
                                product[key] = val

                    if 'category' not in product:
                        breadcrumbs = product_html.select_one(".breadcrumb").find_all("li")
                        for count, crumb in enumerate(breadcrumbs, start=1):
                            if count == len(breadcrumbs) - 1:
                                product['category'] = crumb.select_one('a').text.strip()

                    # get meta data for seo
                    # keywords, description title tag etc
                    description = product_html.find("meta", {"name": "description"})
                    keywords = product_html.find("meta", {"name": "keywords"})
                    if description:
                        product['meta']['description'] = description['content']
                    if keywords:
                        product['meta']['keywords'] = keywords['content']
                    if product_html.select_one('title'):
                        product['meta']['title'] = product_html.select_one('title').text.strip()

                    # grab url of each product image
                    image_container = product_html.select_one('.slider_full_width')
                    if image_container:
                        imgs = image_container.find_all('img')
                        for img in imgs:                            
                            if img.has_attr('data-cfsrc'):                            
                                if img['data-cfsrc'] not in product['images']:
                                    product['images'].append(img['data-cfsrc'])
                            else:
                                if img['src'] not in product['images']:
                                    product['images'].append(img['src'])
                    # get product documents
                    docs = product_html.select_one('.product_documents_detail')
                    if docs:
                        for doc in docs.find_all('a'):
                            product['documents'].append(base_url + doc['href'])
                    # get technical drawings
                    for item in product_html.find_all("div", attrs={'data-title' : True}):
                        if "Product Dimensions" in item['data-title']:
                            for img in item.find_all('img'):
                                if img.has_attr('src'):
                                    product['documents'].append(img['src'])

                    # get related products
                    may_need_products = product_html.select_one('.you-may-need-block')                    
                    if may_need_products:    
                        for prod in may_need_products.find_all('li'):
                            prod_name = prod.select_one('.may_need_desc p a')
                            if not prod_name:
                                prod_name = prod.select_one('.may_need_desc p')

                            rel_product = {
                                'product_code': helper.replace_all(["(",")"], "", prod.select_one('.need_prod_ref').text.strip()),
                                'price': helper.price_str_to_int(prod.select_one('.prod_checkbox_price').text.strip()),
                                'name': ''.join(prod_name.find_all(text=True, recursive=False)).strip(),
                            }

                            for img in prod.find_all('img'):
                                if img.has_attr('src'):
                                    rel_product['image'] = img['src']
                            
                            if rel_product['product_code'] != product['product_code']: 
                                product['related_products'].append(rel_product)

                    may_like = product_html.select_one('.you_may_also_like')                    
                    if may_like:                        
                        for like in may_like.find_all('div', {'class': 'like_prod_item'}):
                            name = like.select_one('.like_prod_name')
                            rel_product = {
                                'price': helper.price_str_to_int(like.select_one('.like_view_price').text.strip()),
                                'name': name.text.strip(),
                            }
                            imgs = like.find_all('img')
                            if imgs:                                
                                for img in imgs:
                                    if img.has_attr('src'):
                                        rel_product['image'] = img['src']
                            
                            for link in name.find_all('a'):
                                rel_product['link'] = link['href']                            
                            product['related_products'].append(rel_product)

                    url_counter += 1
                    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")
                except (AttributeError, ValueError, TypeError) as e:
                    print()
                    print(e)
                    print(loc)
                    print(product)
                    print(url_counter)
                    sleep(5)
                
                # print(product)                
                # Write product infomation to database                
                database.write_product("heat_plumb_products", product) 
            else:
                url_counter += 1
                helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")        
    return True


def trade_plumbing():
    # https://www.tradeplumbing.co.uk/catalog/seo_sitemap/product/
    urls = get_trade_plumbing_urls()
    brands = get_trade_plumbing_brands()    
    session = connection.get_tor_session()
    ua = connection.get_user_agent()    
    print("Total Urls: {}".format(len(urls)))
    url_counter = 0
    helper.progress(url_counter, len(urls),"Scraping Product Data", "Complete")

    for url in urls:
        product = {
            'url': url,
            'height': 0,
            'width': 0,
            'depth': 0,
            'material': '',
            'colour': '',
            'price': 0,
            'meta': {}
        }

        if url_counter < 5:  # testing only
            # Reset ip and user agent to avoid being blocked
            if url_counter % random.choice([3, 9, 25, 50, 100, 250]) == 0 and url_counter != 0:
                ua = connection.get_user_agent()
                connection.renew_tor_connection()
                session = connection.get_tor_session()
            try:
                prod_page = session.get(url, headers={'user-agent': ua})
                product_html = create_soup(prod_page.content)
                product_name = product_html.select_one('.product-name > h1')
                product_code = product_html.select_one('.product-sku > span')
                del_price = product_html.select_one('.delRate')
                specs_table = product_html.select_one('#product-attribute-specs-table')
                price = product_html.find("meta", {"itemprop": "price"})
                details = product_html.select_one("#description-container")                          
                product['name'] = product_name.text.strip() if product_name else "No Product Name"                
                product['product_code'] = product_code.text.strip() if product_code else "No Product Code"
                product['price'] = float(price.attrs['content']) if price else "Not Found"
                product['brand'] = brands[product['name'].split(" ")[0]]

                if del_price:
                    del_price = del_price.text.strip()                    
                    product['delivery_price'] = 0 if del_price == "Free" else helper.price_str_to_int(del_price) 
                    product['price'] = float("{0:.2f}".format(product['price'] + product['delivery_price']))

                if specs_table:
                    for row in specs_table.find_all("tr"):
                        label = row.select_one(".label").text.strip().lower()
                        value = row.select_one(".data").text.strip()

                        if value.isdigit():
                            value = float(value)

                        if label == "sku":
                            continue

                        product[label] = value

                if details:
                    list_items = details.find_all("li")
                    for li in list_items:
                        items = li.text.strip().split(":")
                        if len(items) > 1:
                            product[items[0]] = items[1].strip()                                     

                # get meta data for seo
                # keywords, description title tag etc
                description = product_html.find("meta", {"name": "description"})
                keywords = product_html.find("meta", {"name": "keywords"})
                if description:
                    product['meta']['description'] = description['content']
                if keywords:
                    product['meta']['keywords'] = keywords['content']
                if product_html.select_one('title'):
                    product['meta']['title'] = product_html.select_one('title').text.strip()

                if 'category' not in product:
                        breadcrumbs = product_html.select_one(".breadcrumb").find_all("li")
                        for count, crumb in enumerate(breadcrumbs, start=1):
                            if count == len(breadcrumbs) - 1:
                                product['category'] = crumb.select_one('a').text.strip()

                url_counter += 1
    #             helper.progress(url_counter, len(urls), "Scraping Product Data", "Complete")
            except AttributeError as e:
                print(e, url)
                print()
            print()
            print(product)
            # Write product infomation to database
            #database.write_product("trade_plumbing_products", product)

    return True


def get_trade_plumbing_urls():    
    file_name = './urls/trade-plumbing-urls.json' 

    # Open and return urls if file exists
    if os.path.isfile(file_name):
        with open(file_name) as f:
            return json.load(f)         
    else:        
        session = connection.get_tor_session()
        urls = []
        url_counter = 0
        helper.progress(url_counter, 300,"Getting urls", "Complete")
        for num in range(1,300):
            # Get current page content
            url = "https://www.tradeplumbing.co.uk/catalog/seo_sitemap/product/?p=" + str(num)            
            request = session.get(url, headers={'user-agent': connection.get_user_agent()})
            
            if 200 != request.status_code:
                return False

            soup = create_soup(request.content)
            # Extract urls from list of links
            for link in soup.select_one(".sitemap").find_all('a'):
                urls.append(link['href'])
            
            url_counter += 1
            helper.progress(url_counter, 300, "Getting urls", "Complete")

        # Create dir if needed
        if not os.path.exists('./urls'):
            os.makedirs('./urls')    
        # Save urls to json file
        with open(file_name, 'w') as outfile:
            json.dump(urls, outfile)

        return urls


def get_trade_plumbing_brands():
    file_name = './urls/trade-plumbing-brands.json'

    # Open and return urls if file exists
    if os.path.isfile(file_name):
        with open(file_name) as f:
            return json.load(f)
    else:
        session = connection.get_tor_session()
        brands = {}        
        
        # Get current page content
        url = "https://www.tradeplumbing.co.uk/bathrom-heating-brands.html"
        request = session.get(url, headers={'user-agent': connection.get_user_agent()})

        if 200 != request.status_code:
            return False

        soup = create_soup(request.content)
        links = soup.select_one("#amshopby-page-container .subcategory-holder").find_all('a')
        url_counter = 0
        helper.progress(url_counter, len(links), "Getting Brands", "Complete")
        # Extract urls from list of links
        for link in links:
            link_text = link.text.strip()             
            brands[link_text.split(" ")[0]] = link_text
            url_counter += 1
            helper.progress(url_counter, len(links),"Getting Brands", "Complete")
        
        # Add brands that are not under the shop by page
        brands['Mira'] = "Mira"

        # Create dir if needed
        if not os.path.exists('./urls'):
            os.makedirs('./urls')
        # Save urls to json file
        with open(file_name, 'w') as outfile:
            json.dump(brands, outfile)

        return brands


def trade_plumbing_gs():
    session = connection.get_tor_session()
    ua = connection.get_user_agent()
    url = 'https://www.tradeplumbing.co.uk/feeds/GoogleShopping.xml'  
    local_filename = url.split('/')[-1]    
    r = session.get(url , headers={'user-agent': ua}, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
