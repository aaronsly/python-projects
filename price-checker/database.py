import mysql.connector, json, helper
from mysql.connector import connection, errorcode
from configparser import ConfigParser


def db_config(filename='config.ini', section='mysql'):
    """ Read database configuration file and return a dictionary object
    :param filename: name of the configuration file
    :param section: section of database configuration
    :return: a dictionary of database parameters
    """
    # create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(filename)

    # get section, default to mysql
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception(
            '{0} not found in the {1} file'.format(section, filename))

    return db


def connect():
    """ Connects to database and returns connection object"""
    try:
        cnx = mysql.connector.connect(**db_config())
        return cnx
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
    else:
        cnx.close()

if __name__ == '__main__':
    connect()

def write_product(table, product):
    try:
        cnx = connect()
        cursor = cnx.cursor()
        
        query = ("REPLACE INTO " + table +
        "(url, code, brand, name, category, width, height, depth, finish, material, price, images, attributes, meta, related_products, documents) "
        "Values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)")

        data = (
            product['url'],
            product['product_code'],
            product['brand'],
            product['name'],
            product['category'],
            helper.str_to_int(product['width']),
            helper.str_to_int(product['height']),
            helper.str_to_int(product['depth']),
            product['colour'],
            product['material'],
            product['price'],
            json.dumps(product['images']),
            json.dumps(product),
            json.dumps(product['meta']),            
            json.dumps(product['related_products']),
            json.dumps(product['documents']),
        )
        
        cursor.execute(query, data)
        cnx.commit()
        cursor.close()
        cnx.close()    
    except Exception as e:
        print(e)
        print(data)
        print(product)
    return True

def write_ignore_url(table,url):
    cnx = connect()
    cursor = cnx.cursor()
    query = ("REPLACE INTO " + table +
            " (url) "
            "VALUES (%s);")
    data = (url,)
    cursor.execute(query, data)
    cnx.commit()
    cursor.close()
    cnx.close()
    return True

def get_table_contents(table):
    cnx = connect()
    cursor = cnx.cursor(dictionary=True)
    cursor.execute("SELECT * FROM " + table )

    # cursor.close()
    # cnx.close()

    return cursor.fetchall()
