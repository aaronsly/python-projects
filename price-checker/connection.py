import sys, stem, requests, time, random, stem.process
from stem import Signal
from stem.control import Controller
from fake_useragent import UserAgent
from bs4 import BeautifulSoup as BS

def check_tor_connection():
    try:
        with Controller.from_port(port=9051) as controller:
            if controller.is_alive():
                print("""\n***********************************************************
*      Price Matcher Running      *
***********************************************************\n""")
                return True
            else:
                print("""Tor connection is not active please check connection and try again.""")
                sys.exit()
    except stem.SocketError:
        print("""This program uses Tor which is not currently active.
Please connect to the Tor Network with ControlPort 9051 enabled and try again.""")
        sys.exit()

def get_tor_session():
    session = requests.session()
    # Tor uses the 9050 port as the default socks port
    session.proxies = {'http':  'socks5://127.0.0.1:9050',
                       'https': 'socks5://127.0.0.1:9050'}
    return session

def renew_tor_connection():
    with Controller.from_port(port=9051) as controller:
        if controller.is_newnym_available():
            controller.authenticate(password="essentialWebScrape!")
            controller.signal(Signal.NEWNYM)
            time.sleep(controller.get_newnym_wait())

def kill_tor():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate(password="essentialWebScrape!")
        controller.signal(Signal.TERM)
        print("Tor killed")

def get_user_agent():
    return UserAgent().random


def get_proxies():
    proxies = []
    session = requests.session()
    ua = UserAgent().random    
    proxies_response = session.get("https://www.socks-proxy.net/", headers={'user-agent': ua})

    if proxies_response.status_code == 200:
        proxy_soup = BS(proxies_response.content, "html.parser")
        proxies_table = proxy_soup.find(id='proxylisttable')

        # Save proxies in the array
        for row in proxies_table.tbody.find_all('tr'):
            cells = row.find_all('td')
            #if cells[4].text == 'Socks5':
            proxies.append({
                'ip':   cells[0].string,
                'port': cells[1].string,
                'socks': cells[4].string
            })
    return proxies


# def random_proxy(proxies):
#   return random.randint(0, len(proxies) - 1)


# def get_session():
#     proxies = get_proxies()    
#     proxy = proxies[random_proxy(proxies)]
#     session = requests.session()
#     session.proxies = {'http':  '{}://{}:{}'.format(proxy['socks'], proxy['ip'], proxy['port']),
#                        'https': '{}://{}:{}'.format(proxy['socks'], proxy['ip'], proxy['port'])}
#     return session


def get_proxy_session(test_url):
    proxies = get_proxies()
    random.shuffle(proxies)
    ua = get_user_agent()
    print("Searching for a proxy that works...")
    for proxy in proxies:
        try:
            session = requests.session()
            session.proxies = {'http':  '{}://{}:{}'.format(proxy['socks'], proxy['ip'], proxy['port']),
                       'https': '{}://{}:{}'.format(proxy['socks'], proxy['ip'], proxy['port'])}

            session.headers.update({'user-agent': ua})
            request = session.get(test_url, timeout=30)

            if 200 != request.status_code:
                print(request)
                print("Still searching...")
                continue
            
            print(session.get("http://httpbin.org/ip", headers={'user-agent': ua},timeout=30).text)
            return session

        except requests.exceptions.RequestException as e:            
            print("This is not the proxy you are looking for!")
            print(e)
            continue
