import requests
import BeautifulSoup

url = input("Enter url to scrape: ")

request = requests.get(url)
status = request.status_code

if status == 200 : 
    print(status)
    print(request.headers)

    f = open("scrape-test.txt", "w")
    f.write(request.text)
    f.close()
else :    
    print( "Server Response: {}".format(status) )