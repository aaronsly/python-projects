
# make list for items
shopping_list = []

def show_help():
    # print instructions
    print("What should we pick up at the store?")
    print("""
Enter 'DONE' to stop adding items.
Enter 'HELP' for this help.
Enter 'SHOW' to see your current list items.
""")

def show_list():
    # print list
    print("Here's your list:")
    for item in shopping_list:
        print(item)

def add_to_list(new_item):
    #add new items to list
    shopping_list.append(new_item)
    print("Added {}. List now has {} items.".format(new_item,len(shopping_list)))

def main():
    show_help()

    # create infinate loop
    while True:
        # ask for new items
        new_item = input("> ")
        # be able to quit
        if new_item == 'DONE':
            break
        elif new_item == 'HELP':
            show_help()
            continue
        elif new_item == 'SHOW':
            show_list()
            continue
        
        add_to_list(new_item)    

    show_list()

main()