import xml.etree.ElementTree as ET

import csv,os


def app():
    old = ET.parse('old.xml')
    new = ET.parse('GoogleShopping_full.xml')

    newvals = {}
    newroot = new.getroot()
    csvrows = []
    csvlinks = []

    for newel in newroot.iter('item'):
        for newNode in newel:
            if newNode.tag == '{http://base.google.com/ns/1.0}id':
                sku = newNode.text
            if newNode.tag == 'link':
                link = newNode.text

        newvals[sku] = link

    i = 1
    oldroot = old.getroot()
    for el in oldroot.iter('item'):
        row = {}
        linkrow = {}

        for node in el: 
            if node.tag == 'link':
                linkrow['url'] = node.text
            if node.tag == 'title':
               oldLink = node.text.replace(' ', '-').replace('(', '').replace(')', '').replace("+", '-').replace('"','').replace('/','-').replace('---', '-') + '.html'
            if node.tag == '{http://base.google.com/ns/1.0}id':
                oldSku = node.text.replace('-THDF', '').replace('-SDF', '')

        if oldSku in newvals:
            row['store_id'] = 23
            row['id_path'] = 'hbcustom/00{}'.format(i)
            row['request_path'] = oldLink.replace('https://www.theheatingboutique.co.uk', '')
            row['target_path'] = newvals[oldSku].replace('https://www.theheatingboutique.co.uk/', '')
            row['options'] = 'RP'
            csvrows.append(row)
            csvlinks.append(linkrow)
            i = i + 1

    print(csvrows)

    if not os.path.exists('./output'):
        os.makedirs('./output')

    file_name = './output/redirects.csv'
    headings = ['store_id','id_path','request_path','target_path','options']
    try:
        # create and write to file based on category
        with open(file_name, "w", newline="", encoding='utf-8') as f:
            writer = csv.DictWriter(f, headings)
            writer.writeheader()
            writer.writerows(csvrows)

    except PermissionError as e:
        print(e)
    except UnicodeEncodeError as e:
        print(e)
    else:
        print('')

    file_name2 = './output/old-urls.csv'
    headings2 = ['url']
    try:
        # create and write to file based on category
        with open(file_name2, "w", newline="", encoding='utf-8') as f:
            writer = csv.DictWriter(f, headings2)
            writer.writeheader()
            writer.writerows(csvlinks)

    except PermissionError as e:
        print(e)
    except UnicodeEncodeError as e:
        print(e)
    else:
        print('')

app()
